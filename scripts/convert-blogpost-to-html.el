(require 'ox)

(defun remove-internal-links ()
  "Remove internal links but keep their names."
  (interactive)
  (goto-char (point-min))
  (while (re-search-forward "\\[\\[id:.*?\\]\\[\\(.*?\\)\\]\\]" nil t)
    (replace-match "\\1")))

(defun convert-blogpost-to-html ()
  "Converts org files with the 'blogpost' filetag to HTML body content and saves them as HTML files."
  (interactive)
  (let* ((org-files (directory-files-recursively "~/org/roam/" "\\.org$"))
         (output-dir "~/repos/oscean-blog/src/inc/blog/")
         (export-options '(:html-postamble nil :html-preamble nil :body-only t :exclude-tags '("toc"))))

    (dolist (org-file org-files)
      (with-temp-buffer
        (insert-file-contents org-file)
        (when (string-match "^\\(#\\+filetags:.*\\)blogpost" (buffer-string))
          (save-excursion
            (goto-char (point-min))
            (let ((title (get-property "title"))
                  (html-file (concat output-dir (file-name-base org-file) ".htm")))
              (remove-internal-links)
              (org-export-to-file 'html html-file nil nil nil export-options)
              (message (concat "Converted " title " to HTML body content: " html-file)))))))))

(defun get-property (property)
  "Extracts the value of the specified property from the current buffer."
  (when (re-search-forward (concat "^\\(#\\+" property ":\\s-+\\)\\(.+\\)") nil t)
    (match-string 2)))

(convert-blogpost-to-html)
