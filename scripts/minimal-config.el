;; Set up package.el and add MELPA repository
(setq package-enable-at-startup nil)
(require 'package)
(setq package-user-dir "/tmp/emacs-package-dir")
(add-to-list 'package-archives '("melpa" . "https://melpa.org/packages/"))

;; Initialize packages and install necessary packages
(package-initialize)
(unless (package-installed-p 'org-roam)
  (package-refresh-contents)
  (package-install 'org-roam))

;; Load additional libraries and your script
(require 'ox)
(require 'org-roam)
;; Set org-roam directory
(setq org-roam-directory "~/org/roam/")
(load-file "convert-blogpost-to-html.el")
