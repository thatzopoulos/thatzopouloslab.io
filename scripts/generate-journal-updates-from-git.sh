#/bin/sh
# Generate journal entries for which day git added a new html file, in reverse order.
# Can't tell when a page was updated which is better than nothing
git ls-tree -r --name-only HEAD ../site/*.html | while read filename; do
 printf "%-10s %-25s Created page for %s\n" "$(git log --format="%ad" --date=format:"%Y-%m-%d" -- "$filename" | head -n 1)" "$(echo "${filename%.*}" | tr '_' ' ' | tr '[:lower:]' '[:upper:]')" "${filename%.*}"
done | sort -r