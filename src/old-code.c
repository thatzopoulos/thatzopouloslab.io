void
fpnow(FILE *f, Lexicon *lex, Journal *jou)
{
	int i, projects_len = 0;
	char *pname[56], *pfname[56];
	double sum_value = 0, pval[56], pmaxval = 0;
	time_t now;
	time(&now);
	for(i = 0; i < 14; ++i) { /* This is supposed to print 56 entries but i have around 10 entries :( */
		int index = 0;
		Log l = jou->logs[i];
		if(arveliedays(l.date) < 56)
			break;
		if(l.code % 10 < 1)
			continue;
		index = afnd(pname, projects_len, l.term->name);
		if(index < 0) {
			index = projects_len;
			pname[index] = l.term->name;
			pfname[index] = l.term->filename;
			pval[index] = 0;
			projects_len++;
		}
		pval[index] += l.code % 10;
		sum_value += l.code % 10;
	}
	/* find most active with a photo */
	for(i = 0; i < projects_len; ++i) {
		if(finddiary(jou, findterm(lex, pname[i]), 0) && pval[i] > pmaxval)
			pmaxval = pval[i];
	}
	for(i = 0; i < projects_len; ++i) {
		if(pval[i] != pmaxval)
			continue;
		fplogpict(f, finddiary(jou, findterm(lex, pname[i]), 0), 1);
		break;
	}
	fprintf(
		f,
		"<p>This data shows the distribution of <b>%.0f hours over %d projects</b>, "
		"recorded during the last %d days, for an average of %.1f work hours per day "
		"and %.1f work hours per project.</p>",
		sum_value,
		projects_len,
		56,
		sum_value / 56,
		sum_value / projects_len);
	fputs("<ul style='columns:2'>", f);
	for(i = 0; i < projects_len; ++i) {
		fputs("<li>", f);
		fprintf(f, "<a href='%s.html'>%s</a> %.2f&#37; ", pfname[i], pname[i], pval[i] / sum_value * 100);
		fputs("</li>", f);
	}
	fputs("</ul>", f);
	fprintf(f, "<p>Last generated on %s(" LOCATION ").</p>", ctime(&now));
}


void
fpcalendar(FILE *f, Journal *jou)
{
	int i, last_year = 0;
	fputs("<ul>", f);
	for(i = 0; i < jou->len; ++i) {
		Log *l = &jou->logs[i];
		if(jou->logs[i].rune != '+')
			continue;
		if(last_year != sint(l->date, 2))
			fputs("</ul><ul>", f);
		fprintf(f, "<li><a href='%s.html' title='", l->term->filename);
		fpRFC2822(f, arvelietime(EPOCH, l->date), 0);
		fprintf(f, "'>%s</a> %s</li>", l->date, l->name);
		last_year = sint(jou->logs[i].date, 2);
	}
	fputs("</ul>", f);
}