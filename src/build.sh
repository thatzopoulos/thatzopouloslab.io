#!/bin/bash

# Lint
clang-format -i main.c

# Cleanup
rm -rf ../site
mkdir ../site

# Projects
# cc projects/marbles/marbles.c -std=c89 -Os -DNDEBUG -g0 -s -Wall -Wno-unknown-pragmas -o marbles

# Linux(debug)
# cc -std=c89 -DDEBUG -Wall -Wno-unknown-pragmas -Wpedantic -Wshadow -Wuninitialized -Wextra -Werror=implicit-int -Werror=incompatible-pointer-types -Werror=int-conversion -Wvla -g -Og -fsanitize=address -fsanitize=undefined main.c -o blog

# Linux(fast)
cc main.c -std=c89 -Os -DNDEBUG -g0 -s -Wall -Wno-unknown-pragmas -o blog

# RPi
# tcc -Wall main.c -o blog

# Plan9
# pcc main.c -o blog

# Valgrind
# gcc -std=c89 -DDEBUG -Wall -Wpedantic -Wshadow -Wuninitialized -Wextra -Werror=implicit-int -Werror=incompatible-pointer-types -Werror=int-conversion -Wvla -g -Og main.c -o blog
# valgrind ./blog

# Build Size
# echo "$(du -b ./blog | cut -f1) bytes written"

# Run
./blog

# Cleanup
rm -f ./blog
