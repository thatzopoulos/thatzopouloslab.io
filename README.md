# Oscean

Personal Dev Site. Originally a fork of the C version of the codebase that ran the Oscean Wiki https://wiki.xxiivv.com/site/home.html

## Build
```
cd src/
./build.sh
```

## Example Meta Page

```
WEBSITE
	HOST : HOME
	BREF : Notes on the website for my own reference.
	BODY
		<p>The code that generates this website is based on the original C implementation of {https://github.com/XXIIVV/oscean Oscean}</p>
		<h3>Templating</h3>
		<table border='1'>
		<tr><th>plaint text</th><th>result</th><th>name</th></tr>
		<tr><td><code>&#123;website&#125;</code></td><td>{website}</td><td>send</td></tr>
		<tr><td><code>&#123;https://athanasi.us&#125;</code></td><td>{https://athanasi.us}</td><td>link</td></tr>
		<tr><td><code>&#123;https://athanasi.us link description&#125;</code></td><td>{https://athanasi.us link description}</td><td>link named</td></tr>
		<tr><td><code>&#123;^img refs/mos6502.jpg 100&#125;</code></td><td>{^img refs/mos6502.jpg 100}</td><td>image</td></tr>
		</table>
	LINK
		The Four Horseman : https://www.youtube.com/watch?v=3KCbqhJt16k

```